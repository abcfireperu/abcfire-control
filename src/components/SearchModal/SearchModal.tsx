import React, { useState } from 'react';
import { IonContent, IonHeader, IonTitle, IonToolbar, IonModal, IonSearchbar, IonList, IonItem, IonLabel, IonThumbnail, IonIcon, IonSpinner } from '@ionic/react';

import './SearchModal.css'

interface SearchModelResult {
  id: number | string,
  label: string
}

interface SearchModalProps {
  title: string,
  isOpen: boolean,
  onSelect: (result: SearchModelResult) => void;
}

const fireExtinguishers = [
  { id: '1', label: 'Extintor 1' },
  { id: '2', label: 'Extintor 2' },
  { id: '3', label: 'Extintor 3' },
  { id: '4', label: 'Extintor 4' },
]

const SearchModal: React.FC<SearchModalProps> = ({ title, isOpen, onSelect }) => {
  const [isLoading, setIsLoading] = useState(false);

  return (
    <IonModal isOpen={isOpen}>
      <IonHeader translucent>
        <IonToolbar>
          <IonTitle>{title}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonSearchbar placeholder="Buscar"></IonSearchbar>
      <IonContent>
        {isLoading ? <IonSpinner /> : <IonList>
          {fireExtinguishers.map(({ id, label }) =>
            <IonItem key={id} button onClick={(e) => onSelect({ id, label })}>
              <IonThumbnail slot="start" onClick={(e) => e.stopPropagation()}>
                <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==" />
              </IonThumbnail>
              <IonLabel>
                <h3>{label}</h3>
                <p>Informacion adicional</p>
              </IonLabel>
              <IonIcon slot="end" />
            </IonItem>
          )}
        </IonList>}

      </IonContent>
    </IonModal>
  );
};

export default SearchModal;