import React, { useState } from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab1.css';
import SearchModal from '../components/SearchModal';

const Tab1: React.FC = () => {
  const [searchTypeOpen, setSearchTypeOpen] = useState(true)

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 1</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 1</IonTitle>
          </IonToolbar>
        </IonHeader>
        <ExploreContainer name="Tab 1 page" />
        <SearchModal isOpen={searchTypeOpen} title="Seleccionar tipo" onSelect={(result) => {
          setSearchTypeOpen(false)
          console.log(result)
        }}/>
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
